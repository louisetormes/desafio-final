export type Pessoa = {
    cpf: string;
    name: string;
    img: string;
    idade: string;
    phone: string;
}

export type PessoaListItem = Pick<Pessoa, "cpf" | "name" | "img">;