import { Pessoa } from "../types";

type Response = { results: { name: string } [] };

export type ApiPessoa = Response['results'][0]

const baseURL = 'https://randomuser.me/api'

export const fetchPessoa = () => {
    const url = `${baseURL}?limit=100000&offset=0`;
    return fetch(url)
    .then((response) => response.json())
    .then((data: Response) => data.results);
};

export const fetchPessoas = (name: string) => {
    const url = `${baseURL}/${name}`;
  return fetch(url)
    .then((response) => response.json())
    .then((data: any) => ({
      cpf: data.cpf,
      name: name,
      img: data.sprites.other["official-artwork"].front_default,
      idade: data.idade,
    } as Pessoa));
}