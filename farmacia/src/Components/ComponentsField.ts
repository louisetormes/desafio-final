import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import { DataGrid, GridColumns } from "@mui/x-data-grid";
import  fetchPessoa, fetchPessoa   from "./services/fetch-pessoa";
import { Pessoa, PessoaListItem }  from "../types";

import {
  Autocomplete,
  Card,
  CardContent,
  CardMedia,
  List,
  ListItem,
  ListItemText,
  TextField,
  Typography,
} from "@mui/material";
import { Container, SxProps, width } from "@mui/system";


 function ComponentField() extends React.Component {
    <React.Fragment>
    const columns: GridColumns<PessoaListItem> = [
        {
          field: "cpf",
          headerName: "Number",
          flex: 1,
        },
        {
          field: "name",
          headerName: "Pessoa",
          flex: 2,
        },
        {
          field: "img",
          headerName: "Image",
          flex: 2,
          renderCell: (params) => <img src={params.value}></img>,
        },
      ];
      </React.Fragment>
      }
}


export default ComponentField;