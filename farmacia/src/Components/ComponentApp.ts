import { useEffect, useState } from "react";
import { fetchPessoa, fetchPessoas } from "../services/fetch-pessoa"

import { Pessoa, PessoaListItem } from "../types";

import {
  Autocomplete,
  Card,
  CardContent,
  CardMedia,
  List,
  ListItem,
  ListItemText,
  TextField,
  Typography,
} from "@mui/material";
import { Container, SxProps, width } from "@mui/system";

function ComponentApp() {
    const [pessoaName, setPessoaName] = useState("");
    const [pessoa, setPessoa] = useState<Pessoa | null>(null);
    const [pessoaList, setPessoaList] = useState<PessoaListItem[]>([]);
  
    const pessoaChosen = Boolean(pessoa);
  
    const isPessoa = (p: Pessoa | null): p is Pessoa => pessoaChosen;
  
    useEffect(() => {
      fetchPessoa()
        .then((pessoa) => setPessoaList(pessoa));
    }, []);
  
    useEffect(() => {
      if (!pessoaName) return;
      fetchPessoa(pessoaName).then((p) => {
        console.log(p);
        setPessoa(p);
      });
    }, [pessoaName]);
    return (
      
        {isPessoa(pessoa) && (
          <Box>
            <Card sx={{ minWidth: 400 }}>
              <CardMedia
                sx={{ maxWidth: 400 }}
                component="img"
                alt={`Imagem da pessoa ${pessoa.name}`}
                height="100%"
                image={pessoa.img}
              />
              <CardContent>
                <Typography variant="h5" component="div">
                  {pessoa.name}
                </Typography>
                <List>
                  {Object.keys(pessoa).map((k) => {
                    if (k === "img") return null;
                    const key = k as keyof Pessoa;
                    return (
                      <ListItem sx={{ textAlign: "left" }}>
                        <ListItemText primary={`${key}: `} />
                        <ListItemText primary={pessoa[key]} />
                      </ListItem>
                    );
                  })}
                </List>
              </CardContent>
            </Card>
          </Box>
        )}
        <Box
          display="flex"
          justifyContent="space-around"
          alignItems="center"
          flexDirection="column"
          component="main"
          gap={5}
          className="App"
        >
          <Typography variant="h2">Pessoa</Typography>
          <Autocomplete
            id="select-pessoa"
            options={pessoaList}
            sx={{ width: 350 }}
            getOptionLabel={(option) => option.name}
            onChange={(_, newValue) => setPessoaName(newValue?.name || "")}
            renderInput={(params) => (
              <TextField {...params} label="Selecione a pessoa:"/>
            )}


            export default ComponentApp;